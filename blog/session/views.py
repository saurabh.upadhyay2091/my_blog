from django.shortcuts import render


# Create your views here.
def setsession(request):
    request.session["name"] = "saurabh"
    request.session["lname"] = "upadhyay"
    return render(request, "session/setsession.html")


def getsession(request):
    # name = request.session['name']
    name = request.session.get("name")
    lname = request.session.get("lname")
    return render(request, "session/getsession.html", {"name": name, "lname": lname})


#
# def delsession(request):
# 	if 'name' in request.session:
# 		del request.session['name']
# 	return render(request, 'session/delsession.html')


def delsession(request):
    request.session.flush()
    return render(request, "session/delsession.html")
