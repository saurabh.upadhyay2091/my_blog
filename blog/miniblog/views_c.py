from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import Group
from django.shortcuts import HttpResponseRedirect, render

from .forms import LoginForm, PostForm, SignUpForm
from .models import Post


def home(request):
    posts = Post.objects.all()
    return render(request, "miniblog/home.html", {"posts": posts})


def about(request):
    return render(request, "miniblog/about.html")


def contact(request):
    return render(request, "miniblog/contact.html")


def dashboard(request):
    if request.user.is_authenticated:
        posts = Post.objects.all()
        user = request.user
        full_name = user.get_full_name()

        return render(
            request, "miniblog/dashboard.html", {"posts": posts, "full_name": full_name}
        )
    else:
        return HttpResponseRedirect("/login/")


def user_logout(request):
    logout(request)
    return HttpResponseRedirect("/")


def user_signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            messages.success(request, "Congratulations!! You have become an Author.")
            user = form.save()
    else:
        form = SignUpForm()
    return render(request, "miniblog/signup.html", {"form": form})


def user_login(request):
    if not request.user.is_authenticated:
        if request.method == "POST":
            form = LoginForm(request=request, data=request.POST)
            if form.is_valid():
                uname = form.cleaned_data["username"]
                upass = form.cleaned_data["password"]
                user = authenticate(username=uname, password=upass)
                if user is not None:
                    login(request, user)
                    messages.success(request, "Logged in Successfully !!")
                    return HttpResponseRedirect("/dashboard/")
        else:
            form = LoginForm()
        return render(request, "miniblog/login.html", {"form": form})
    else:
        return HttpResponseRedirect("/dashboard/")


def add_post(request):
    if request.user.is_authenticated:
        if request.method == "POST":
            form = PostForm(request.POST)
            if form.is_valid():
                title = form.cleaned_data["title"]
                desc = form.cleaned_data["desc"]
                pst = Post(title=title, desc=desc)
                pst.save()
                form = PostForm()
        else:
            form = PostForm()
        return render(request, "miniblog/addpost.html", {"form": form})
    else:
        return HttpResponseRedirect("/login/")
